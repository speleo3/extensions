# coding=utf-8
from jitternodes import JitterNodes
from tests.base import InkscapeExtensionTestMixin, TestCase


class JitterNodesBasicTest(InkscapeExtensionTestMixin, TestCase):
    effect_class = JitterNodes
